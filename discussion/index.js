//EXPRESS SETUP


// 1. Import by using the 'require' directive to get access to the components of
// express package/dependecy

const express = require ('express')

// Use express () function and assign in to an app 
//variabloe to create an express app or app server
const app = express()
// Declare a variable for the port of the server
const port = 3000;

//Middlewares
//These 2 .use are essential in express
//Allows ur app to read json format data
app.use(express.json())
//Allows ur app to read data forms
app.use(express.urlencoded({extended: true}))

//Routes

//Get Request Route
app.get('/',(req,res) => {
    //logic , once the route is accessed it will send a string response
    res.send('Hello')

})

// app.get('/user',(req,res) => {
//     //logic 
//     res.send('Hello User')

// })

//Register user route
//
let users = [];

app.post('/register',(req,res) => {
    //logic 
   if(req.body.username !== " " && req.body.password !== " " ){
    users.push(req.body)
    console.log(users)
    res.send(`Successfully registered User ${req.body.username}`)
   }
   else{
    res.send(`Username and Password cannot be left blank`)
   }
}) 
//will work if Middleware is declared.


//PUT REQUEST ROUTE
app.put('/change-password',(req,res) => {
    //logic 
  let message;

  for(let i = 0; i<=users.length; i++){
    if(req.body.username == users[i].username){
        users[i].password == req.body.password
        message = `User ${req.body.username}'s has been updated`
        break;
    }else { message = 'User does not exist'}
  }
  res.send(message)

})


//ACTIVITY

//
app.get('/home',(req,res) => {
    //logic 
    res.send('Welcome to homepage')

})

//
app.get('/user',(req,res) => {
    //logic 
    res.send(users)

})


//

app.delete('/delete-user',(req,res) => {
    //logic 
  let message;

  for(let i = 0; i<=users.length; i++){
    if(req.body.username == users[i].username ){
       
        message = `User ${req.body.username}'s has been deleted`
        break;
    }else { message = 'User does not exist'}
  }
  res.send(message)

})


app.listen(port, () => console.log(`Server is running at port ${port}`))

